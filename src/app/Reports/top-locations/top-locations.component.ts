import { Component,ViewChild,ElementRef, OnInit } from '@angular/core';
import {ExportExcelService} from 'src/app/eventDataService/export-excel.service';
import { freeAPIService } from '../../services/freeapi.service';
import { EventDetails } from 'src/app/classes/eventdetails';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-top-locations',
  templateUrl: './top-locations.component.html',
  styleUrls: ['./top-locations.component.css']
})
export class TopLocationsComponent implements OnInit {
  @ViewChild('table') table: ElementRef;
  eventDate: string;
  lstTopLocations:Array<EventDetails>;
  constructor(private _freeApiService: freeAPIService) { 
    this.lstTopLocations=new Array<EventDetails>();
  }

  ngOnInit() {
  }
  getEventDate(event: any) {
    this.eventDate = event.target.value;
  }

  getTopLocationlistBasedonDate() {
    this._freeApiService.getTopLocationEventList(this.eventDate)
      .subscribe(teamList => {
        this.lstTopLocations = teamList;
      });
      
  }
  exportToExcel(){
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Top Location Details.xlsx');
  }
}
