import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopProjectTeamsComponent } from './top-project-teams.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from '../../home/home.component';
import { HeaderComponent } from '../../shared/header/header.component';
import { freeAPIService } from '../../services/freeapi.service';
import { LoginComponent } from '../../login/login.component';

 // mock the service
  class MockDummyService extends freeAPIService {
    // mock everything used by the component
  };
describe('TopProjectTeamsComponent', () => {
  let component: TopProjectTeamsComponent;
  let fixture: ComponentFixture<TopProjectTeamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      //declarations: [ TopProjectTeamsComponent ]
      declarations: [TopProjectTeamsComponent, HomeComponent, HeaderComponent],
      imports: [ReactiveFormsModule, RouterTestingModule, HttpClientModule,
        HttpClientTestingModule, HttpModule, FormsModule],
      providers: [LoginComponent, {
        provide: freeAPIService,
        useClass: MockDummyService
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopProjectTeamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
