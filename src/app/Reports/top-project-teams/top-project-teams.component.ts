import { Component,ViewChild,ElementRef, OnInit } from '@angular/core';
import {ExportExcelService} from 'src/app/eventDataService/export-excel.service';
import { freeAPIService } from '../../services/freeapi.service';
import { ViewEventRegistration } from 'src/app/classes/eventregistration';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-top-project-teams',
  templateUrl: './top-project-teams.component.html',
  styleUrls: ['./top-project-teams.component.css']
})
export class TopProjectTeamsComponent implements OnInit {

  @ViewChild('table') table: ElementRef;
  eventDate: string;
  lstTopTeams:Array<ViewEventRegistration>;
  constructor(private _freeApiService: freeAPIService) { 
    this.lstTopTeams=new Array<ViewEventRegistration>();
  }

  ngOnInit() {
  }
  getEventDate(event: any) {
    this.eventDate = event.target.value;
  }
  
  getTopTeamlistBasedonDate(){
    this._freeApiService.getTopProjectTeamList(this.eventDate)
    .subscribe(eventList => {
      this.lstTopTeams = eventList;
    });
    
  }
  exportToExcel(){
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Top Project Team Details.xlsx');
  }
}
