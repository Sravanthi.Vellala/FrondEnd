import { Component,ViewChild,ElementRef, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import { LoginComponent } from '../../login/login.component';
import { freeAPIService } from '../../services/freeapi.service';
import { ExportExcelService } from '../../eventDataService/export-excel.service';
import { EventDetails } from '../../classes/eventdetails';

@Component({
  selector: 'app-not-attended',
  templateUrl: './not-attended.component.html',
  styleUrls: ['./not-attended.component.css']
})
export class NotAttendedComponent implements OnInit {
  @ViewChild('table') table: ElementRef;
  lstEventDetails:EventDetails;
  constructor(private loginComponent:LoginComponent,private _freeApiService:freeAPIService,private excelService:ExportExcelService)
  {
    this._freeApiService.getNotAttendedEventDetails().subscribe(
      data=>
      {
        this.lstEventDetails=data;
      }
    )
  }

  ngOnInit() {
  }
  exportToExcel(){
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Volunteer Enrollment Details - NotAttended.xlsx');
  }
}
