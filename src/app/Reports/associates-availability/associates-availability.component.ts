import { Component,ViewChild,ElementRef, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import { FutureAvailability } from '../../classes/futureavailability';
import { LoginComponent } from '../../login/login.component';
import { freeAPIService } from '../../services/freeapi.service';
import { ExportExcelService } from '../../eventDataService/export-excel.service';

@Component({
  selector: 'app-associates-availability',
  templateUrl: './associates-availability.component.html',
  styleUrls: ['./associates-availability.component.css']
})
export class AssociatesAvailabilityComponent implements OnInit {
  @ViewChild('table') table: ElementRef;
  lstFutureAvailability:FutureAvailability
  constructor(private loginComponent:LoginComponent,private _freeApiService:freeAPIService,private excelService:ExportExcelService)
  {
    this._freeApiService.getAssociateFutureAvailability().subscribe(
      data=>
      {
        this.lstFutureAvailability=data;
      }
    )
  }

  ngOnInit() {
  }
  exportToExcel(){
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Associates Future Availability.xlsx');
  }
}
