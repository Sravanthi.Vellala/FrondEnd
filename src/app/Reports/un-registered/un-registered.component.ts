import { Component,ViewChild,ElementRef, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import { ViewEventDetails } from '../../classes/viewevents';
import { LoginComponent } from '../../login/login.component';
import { freeAPIService } from '../../services/freeapi.service';


@Component({
  selector: 'app-un-registered',
  templateUrl: './un-registered.component.html',
  styleUrls: ['./un-registered.component.css']
})
export class UnRegisteredComponent implements OnInit {
  @ViewChild('table') table: ElementRef;
  lstEventDetails:ViewEventDetails[];
  associateID:number;
  participationId:number;
  constructor(private loginComponent:LoginComponent,private _freeApiService:freeAPIService)
  {
    this.associateID= 1;
       this.participationId=2;

       let params1: any = {
        'associateID': 1,
        'participationId':2
     };
    this._freeApiService.getUnRegisterEventDetails().subscribe(
      data=>
      {
        this.lstEventDetails=data;
      }
    )
  }
  ngOnInit() {
  }
//   exportToExcel(){
//     this.excelService.exportAsExcelFile(this.lstEventDetails,"Volunteer Enrollment Details - UnRegistered");
//  }
 exportToExcel(){
  const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

  /* save to file */
  XLSX.writeFile(wb, 'Volunteer Enrollment Details - UnRegistered.xlsx');
}
}
