import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { HeaderComponent } from '../shared/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { freeAPIService } from '../services/freeapi.service';
  import { By } from '@angular/platform-browser';
  import { DebugElement } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

  // mock the service
  class MockDummyService extends freeAPIService {
    // mock everything used by the component
  };

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  let userNameEl: DebugElement;
  let passwordEl: DebugElement;
  let submitEl:DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[ReactiveFormsModule,RouterTestingModule,HttpClientModule,
        HttpClientTestingModule],
      declarations: [ LoginComponent,HeaderComponent ],
      providers: [{
        provide: freeAPIService,
        useClass: MockDummyService
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    submitEl = fixture.debugElement.query(By.css('input[id=login]'));
    userNameEl = fixture.debugElement.query(By.css('input[id=username]'));
    passwordEl = fixture.debugElement.query(By.css('input[id=password]'));
    //emailEl = fixture.debugElement.query(By.css('input[type=email]'));

    fixture.detectChanges();
  });
  
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('canLogin returns false when the user is not authenticated', () => {
    localStorage.setItem('token', '12345'); 
    expect(component.onSubmit()).toBeFalsy();
  });

  it('Setting value to input properties on button click', () => {
   // component.enabled = true;
    fixture.detectChanges();
    expect(submitEl).toBeFalsy();
  });

  // it('Entering value in input controls and emit output events', () => {
  //   let user: any;
  //   userNameEl.nativeElement.value = "sravantthi@gmail.com";
  //   passwordEl.nativeElement.value = "test";
  //   // Subscribe to the Observable and store the user in a local variable.
  //   component.onSubmit.subscribe((value) => user = value);
  //   // This sync emits the event and the subscribe callback gets executed above
  //   submitEl.triggerEventHandler('click', null);
  //   // Now we can check to make sure the emitted value is correct
  //   expect(user.username).toBe("sravanthi@gmail.com");
  //   expect(user.password).toBe("test");
  // });

});
