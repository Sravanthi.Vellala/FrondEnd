import { Component, OnInit } from '@angular/core';
import {LoginComponent} from '../../login/login.component';
import { freeAPIService } from '../../services/freeapi.service';
import { AssociateDetails } from '../../classes/associatedetails';
import { EventRegistration } from '../../classes/eventregistration';
@Component({
  selector: 'app-view-registration',
  templateUrl: './view-registration.component.html',
  styleUrls: ['./view-registration.component.css']
})
export class ViewRegistrationComponent implements OnInit {
id:number;
  lstRegistrationDetails:EventRegistration[];
  constructor(private loginComponent:LoginComponent,private _freeApiService:freeAPIService)
  {
    this.id=1;
    this._freeApiService.getEventRegistrations(this.id).subscribe(
      data=>
      {
        this.lstRegistrationDetails=data;
      }
    )
  }

  
  ngOnInit(){
    this.id=1;
    this._freeApiService.getEventRegistration().subscribe(
      data=>
      {
        this.lstRegistrationDetails=data;
      }
    )
  }

  onUnRegister(event) {
   // event.EventStatus = "Approved";
    this._freeApiService.putUnRegister(event.Id, event).subscribe(
      data => {
        //this.id=1;
        this._freeApiService.getEventRegistration().subscribe(
          data=>
          {
            this.lstRegistrationDetails=data;
          }
        )
      }
    )
    
}
}
