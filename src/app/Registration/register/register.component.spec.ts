import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { freeAPIService } from '../../services/freeapi.service';
import { HomeComponent } from '../../home/home.component';
import { HeaderComponent } from '../../shared/header/header.component';
import { LoginComponent } from '../../login/login.component';
import { AssociateRegistrationComponent } from '../associate-registration/associate-registration.component';
import { BulkRegistrationComponent } from '../bulk-registration/bulk-registration.component';

 // mock the service
  class MockDummyService extends freeAPIService {
    // mock everything used by the component
  };
xdescribe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    //  declarations: [ RegisterComponent ]
    declarations: [ RegisterComponent,HomeComponent,HeaderComponent,AssociateRegistrationComponent,BulkRegistrationComponent ],
    imports: [ReactiveFormsModule, RouterTestingModule, HttpClientModule,
      HttpClientTestingModule,HttpModule,FormsModule],
    providers: [LoginComponent,{
      provide: freeAPIService,
      useClass: MockDummyService
    }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
