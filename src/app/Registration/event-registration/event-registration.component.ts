import { Component, OnInit } from '@angular/core';
import { ViewEventDetails } from '../../classes/viewevents';
import { LoginComponent } from '../../login/login.component';
import { freeAPIService } from '../../services/freeapi.service';
import { Router } from '@angular/router';
import{Location} from'@angular/common';

@Component({
  selector: 'app-event-registration',
  templateUrl: './event-registration.component.html',
  styleUrls: ['./event-registration.component.css']
})
export class EventRegistrationComponent implements OnInit {
  lstEventDetails:ViewEventDetails[];
  constructor(private loginComponent:LoginComponent,private _freeApiService:freeAPIService,private router: Router,private location:Location)
  {
    this._freeApiService.getEventDetails().subscribe(
      data=>
      {
        this.lstEventDetails=data;
      }
    );
  }

  goBack():void{
    this.location.back();
  }
  ngOnInit() {
  }
  onRegister(detail){
    // this.router.navigate(['Register',detail.eventId]);
    // if(detail.VolunteersRequired >= 10){
    //   window.alert("Required Volunteers are already registered.");
    //   return;
    // }
    // else{
    this.router.navigate(['Register',detail.EventId]);
    // }
  }

  onFutureAvailability(){
    this.router.navigate(['FutureAvailability']);
  }
//   public gotoProductDetails(url, id) {
//     this.router.navigate([url, id]).then( (e) => {
//       if (e) {
//         console.log("Navigation is successful!");
//       } else {
//         console.log("Navigation has failed!");
//       }
//     });
// }

  // ngOnInit() {
  //   this.eventDs.getFavEvents().subscribe(result => {
  //     this.favEvents = result;
  //   });
  // }
  // onCreate(){
  //   this.router.navigate(['SingleEvent']);
  // }
  // onDelete(event)
  // {
  //   this.eventDs.removeFavEvent(event)
  //     .subscribe( data => {
  //       this.favEvents = data;
  //     });
   
  // }

}
