import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeAPIService } from '../../services/freeapi.service';
import{Location} from'@angular/common';
@Component({
  selector: 'app-future-availability',
  templateUrl: './future-availability.component.html',
  styleUrls: ['./future-availability.component.css']
})
export class FutureAvailabilityComponent implements OnInit {
  showMsg: boolean = false;
  constructor(private _freeApiService:freeAPIService,private location:Location)
  {
    this.resetForm();
  }

  ngOnInit() {
  }

  resetForm(form?:NgForm)
  {
    if(form!=null)
    {
    form.resetForm();
    this._freeApiService.futureAvailability= {
      availability:null,
      location:'',
      employeeId:null
    }
    }
  }

  onSubmit(form:NgForm){
 this._freeApiService.postFutureAvailability(form.value).subscribe(res=>{
});
this.showMsg= true;
this.resetForm(form);
this.goBack();
  }

  goBack():void{
    this.location.back();
  }

}
