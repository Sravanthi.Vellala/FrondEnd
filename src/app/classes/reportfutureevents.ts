import { Time } from '@angular/common';

export class ReportFutureEvents {
    beneficiaryName: string;
    councilName: string;
    projectName: string;
    eventCategory: string;
    eventName: string;
    eventDescription: string;
    eventDate: Date;
    startTime: Time;
    endTime: Time;
    volunteersRequired: number;
    pocId: number;
    transportType: string
    boardingPoint: string;
    dropPoint: string;
}