import { Time } from '@angular/common';

export class ViewEventDetails {
    id: number;
    EventId: number;
    locationName: string;
    beneficiaryName: string;
    councilName: string;
    eventName: string;
    eventDescription: string;
    eventDate: Date;
    eventCategory:string;
    //eventDate: string;
    businessUnit: string;
    projectName:string;
    employeeID: number;
    employeeName: string;
    contactNumber: number;
    email: string;
    eventStatus: string;
    transportTypeId: number;
    startTime: Time;
    EndTime: Time;
    Address: Time;
    VolunteersRequired: number;
    TransportType: string;
    BoardinPoint: string;
    DropPoint: string;
    FavEvent: boolean;
    EventCode:string;
    volunteerHours:number;
    travelHours:number;
    livesImpacted:number;
    PocName:string;
    pocId:number
    totalVolunteerHours:number;
    totalTravelHours:number;
    overAllHours:number;
    totalVolunteerCount:number;
    pocIds:string;
    poccontactNumber:string;
    eventMonth:string;
}
export class BusinessUnit{
    id:number;
    businessUnitName:string;
}
