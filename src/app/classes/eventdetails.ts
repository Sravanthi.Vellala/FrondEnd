import { Time } from '@angular/common';
import { JsonProperty } from "json-object-mapper";

export class EventDetails {

  @JsonProperty()
  eventId: number;
  @JsonProperty()
  eventCode: string;
  @JsonProperty()
  eventName: string;
  @JsonProperty()
  eventDescription: string;
  @JsonProperty()
  eventDate: Date;
  @JsonProperty()
  startTime: Time;
  @JsonProperty()
  endTime: Time;
  @JsonProperty()
  locationName: string;
  @JsonProperty()
  eventCategory: string;
  @JsonProperty()
  beneficiaryName: string;
  @JsonProperty()
  projectName: string;
  @JsonProperty()
  councilName: string;
  @JsonProperty()
  volunteersRequired: number;
  @JsonProperty()
  transportTypeId: number;
  @JsonProperty()
  boardingPoint: string;
  @JsonProperty()
  dropPoint: string;
  @JsonProperty()
  pocId: number;
  @JsonProperty()
  eventStatus: string;
  @JsonProperty()
  favEvent: boolean;
  @JsonProperty()
  address: string;
  @JsonProperty()
  isApproved: boolean;
  @JsonProperty()
  isRejected: boolean;
@JsonProperty()
countLocation:number;
  // transportType:string
}
