export class ChartData{

    month:string;
    eventCount:number;
    registrationCount:number;
    projectTeam:string;
    eventDate:Date;
}
