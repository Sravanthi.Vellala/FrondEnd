export class FutureAvailability {
    availability: Date;
    location: String;
    employeeId: number;
}