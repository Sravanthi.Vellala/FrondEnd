import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { freeAPIService } from '../services/freeapi.service';
import { LoginComponent } from '../login/login.component';
import { AssociateDetails } from '../classes/associatedetails';
import { Role } from '../classes/roles';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // currentUser: AssociateDetails;
  // constructor(private router: Router, private _freeApiService: freeAPIService,private login:LoginComponent) { 
  //   this.currentUser = this.login.currentUserValue;
  // }

  ngOnInit() {
  }
  // logout(){
  //   // this._freeApiService.ValidateUser(user).subscribe((result) => {
  //   //  // this.globalResponse = result;
  //   // },
  // }

  currentUser: AssociateDetails;
  admin: string;
  pmo: string;
  poc: string;
  user: string;

    constructor(private router: Router,private _freeApiService:freeAPIService) {
       if(_freeApiService.getToken() == "Admin")
       this.admin="Admin";
       else if(_freeApiService.getToken() == "PMO")
       this.pmo="PMO";
       else if(_freeApiService.getToken() == "POC") 
       this.poc="POC";
       else if(_freeApiService.getToken() == "User") 
       this.user="User";
    }

    // get isAdmin() {
    //     return this.currentUser && this.currentUser.role === Role.Admin;
    // }

    logout() {
       this._freeApiService.removeToken();
       this._freeApiService.removeCurrentUserId();
        this.router.navigate(['/login']);
    }
}
