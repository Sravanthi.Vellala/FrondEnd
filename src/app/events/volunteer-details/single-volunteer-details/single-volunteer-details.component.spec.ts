import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleVolunteerDetailsComponent } from './single-volunteer-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { freeAPIService } from '../../../services/freeapi.service';
import { HomeComponent } from '../../../home/home.component';
import { HeaderComponent } from '../../../shared/header/header.component';
import { LoginComponent } from '../../../login/login.component';

 // mock the service
  class MockDummyService extends freeAPIService {
    // mock everything used by the component
  };
  
describe('SingleVolunteerDetailsComponent', () => {
  let component: SingleVolunteerDetailsComponent;
  let fixture: ComponentFixture<SingleVolunteerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
     // declarations: [ SingleVolunteerDetailsComponent ]
     declarations: [ SingleVolunteerDetailsComponent,HomeComponent,HeaderComponent ],
     imports: [ReactiveFormsModule, RouterTestingModule, HttpClientModule,
       HttpClientTestingModule,HttpModule,FormsModule],
     providers: [LoginComponent,{
       provide: freeAPIService,
       useClass: MockDummyService
     }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleVolunteerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
