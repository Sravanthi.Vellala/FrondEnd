import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureRolesComponent } from './configure-roles.component';
import { HomeComponent } from '../../home/home.component';
import { HeaderComponent } from '../../shared/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { freeAPIService } from '../../services/freeapi.service';

 // mock the service
 class MockDummyService extends freeAPIService {
  // mock everything used by the component
};

describe('ConfigureRolesComponent', () => {
  let component: ConfigureRolesComponent;
  let fixture: ComponentFixture<ConfigureRolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
     // declarations: [  ]
     declarations: [ ConfigureRolesComponent,HomeComponent,HeaderComponent ],
     imports: [ReactiveFormsModule, RouterTestingModule, HttpClientModule,
       HttpClientTestingModule,HttpModule,FormsModule],
     providers: [{
       provide: freeAPIService,
       useClass: MockDummyService
     }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
