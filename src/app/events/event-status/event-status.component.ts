import { Component, OnInit } from '@angular/core';
import { ViewEventDetails } from '../../classes/viewevents';
import { LoginComponent } from '../../login/login.component';
import { freeAPIService } from '../../services/freeapi.service';
import { EventDetails } from '../../classes/eventdetails';

@Component({
  selector: 'app-event-status',
  templateUrl: './event-status.component.html',
  styleUrls: ['./event-status.component.css']
})
export class EventStatusComponent implements OnInit {

  isApproved: boolean = false;
  isRejected: boolean = false;
  lstEventDetails: EventDetails[];

  constructor(private loginComponent: LoginComponent, private _freeApiService: freeAPIService) {
    this._freeApiService.getEventDetails().subscribe(
      data => {
        this.lstEventDetails = data;
        var i: number;
        for (i = 0; i < data.length; i++) {
          if (data[i].eventStatus == "Approved") {
            this.lstEventDetails[i].isApproved = true;
          }
          else if (data[i].eventStatus == "Rejected") {
            this.lstEventDetails[i].isRejected = true;
          }
          else {
            this.lstEventDetails[i].isApproved = false;
            this.lstEventDetails[i].isRejected = false;
          }
        }
      }
    )
  }


  ngOnInit() {
  }
  onApproved(event) {
    this.isApproved = true;
    event.isApproved = this.isApproved;
    event.eventStatus = "Approved";
    this._freeApiService.putEventStatus(event.EventId, event).subscribe(
      data => {
      }
    )
  }

  onRejected(event) {
    this.isRejected = true;
    event.isRejected = this.isRejected;
    event.eventStatus = "Rejected";
    this._freeApiService.putEventStatus(event.EventId, event).subscribe(
      data => {
      }
    )
  }
  toggle = true;
  status = 'Enable';

  enableDisableRule(job) {
    this.toggle = !this.toggle;
    this.status = this.toggle ? 'Enable' : 'Disable';
  }
}
