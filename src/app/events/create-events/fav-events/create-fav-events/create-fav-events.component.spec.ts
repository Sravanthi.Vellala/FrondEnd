import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFavEventsComponent } from './create-fav-events.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { freeAPIService } from '../../../../services/freeapi.service';
import { HomeComponent } from '../../../../home/home.component';
import { HeaderComponent } from '../../../../shared/header/header.component';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { convertToParamMap } from '@angular/router';
import { LoginComponent } from '../../../../login/login.component';


 // mock the service
 class MockDummyService extends freeAPIService {
  // mock everything used by the component
};

xdescribe('CreateFavEventsComponent', () => {
  let component: CreateFavEventsComponent;
  let fixture: ComponentFixture<CreateFavEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
     // declarations: [  ]
     declarations: [ CreateFavEventsComponent,HomeComponent,HeaderComponent ],
     imports: [ReactiveFormsModule, RouterTestingModule, HttpClientModule,
       HttpClientTestingModule,HttpModule,FormsModule],
     providers: [LoginComponent,{
       provide: freeAPIService,
       useClass: MockDummyService
     },
     { provide: ActivatedRoute, useValue: {
      paramMap: of( convertToParamMap( { id: 0 } ) )
  }
}
    ]
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFavEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
