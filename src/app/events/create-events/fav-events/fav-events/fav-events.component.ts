import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ViewEventDataService} from 'src/app/eventDataService/view-event-data.service';
import { ViewEventDetails } from '../../../../classes/viewevents';
import { LoginComponent } from '../../../../login/login.component';
import { freeAPIService } from '../../../../services/freeapi.service';
import { ExportExcelService } from '../../../../eventDataService/export-excel.service';
@Component({
  selector: 'app-fav-events',
  templateUrl: './fav-events.component.html',
  styleUrls: ['./fav-events.component.css']
})
export class FavEventsComponent implements OnInit {

  //favEvents: any[] = [];
  // constructor(private eventDs:ViewEventDataService,private router: Router) {    
	
  // }
  lstViewEvents:ViewEventDetails[];
  associateID:number;
  favEvent:boolean;
  constructor(private loginComponent:LoginComponent,private _freeApiService:freeAPIService,private excelService:ExportExcelService,private favEvents:ViewEventDataService,private router: Router)
  {
    this.favEvent=true;
    this.associateID=1;

    this._freeApiService.getFavEventDetails().subscribe(
      data=>
      {
        this.lstViewEvents=data;
      }
    )
  }

  ngOnInit() {
    // this.eventDs.getFavEvents().subscribe(result => {
    //   this.favEvents = result;
    //});
    this.favEvent=true;
    this.associateID=1;
    this._freeApiService.getFavEventDetails().subscribe(
      data=>
      {
        this.lstViewEvents=data;
      }
    )
  }
  onCreate(event){
   // this.router.navigate(['SingleEvent']);
    //this.router.navigate(['SingleEvent',event.EventId]);
    //this.router.navigate(['CreateFavEvents',event.EventId]);
    this.router.navigate(['CreateFavEvents',event.EventId]);
  }
  onDelete(event)
  {
    // this.eventDs.removeFavEvent(event)
    //   .subscribe( data => {
    //     this.favEvents = data;
    //   });
    this._freeApiService.putRemoveFavourite(event.EventId, event).subscribe(
      data => {
        // this.id=1;
        event.FavEvent=false;
        this.favEvent=true;
    this.associateID=1;
    this._freeApiService.getFavEventDetails().subscribe(
      data=>
      {
        this.lstViewEvents=data;
      }
    )
      }
    )
  }
}
