import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEventsComponent } from './create-events.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule ,HttpTestingController} from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { freeAPIService } from '../../services/freeapi.service';
import { HomeComponent } from '../../home/home.component';
import { HeaderComponent } from '../../shared/header/header.component';
import { SingleEventComponent } from './single-event/single-event.component';
import { BulkUploadComponent } from './bulk-upload/bulk-upload.component';
import { FavEventsComponent } from './fav-events/fav-events/fav-events.component';
import { LoginComponent } from '../../login/login.component';


 // mock the service
  class MockDummyService extends freeAPIService {
    // mock everything used by the component
  };
describe('CreateEventsComponent', () => {
  let component: CreateEventsComponent;
  let fixture: ComponentFixture<CreateEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      //declarations: [  ]
      declarations: [CreateEventsComponent,HomeComponent,HeaderComponent,SingleEventComponent,BulkUploadComponent,FavEventsComponent ],
      imports: [ReactiveFormsModule, RouterTestingModule, HttpClientModule,
        HttpClientTestingModule,HttpModule,FormsModule],
      providers: [LoginComponent,{
        provide: freeAPIService,
        useClass: MockDummyService
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
