import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkUploadComponent } from './bulk-upload.component';

import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { freeAPIService } from '../../../services/freeapi.service';
import { HomeComponent } from '../../../home/home.component';
import { HeaderComponent } from '../../../shared/header/header.component';


class MockDummyService extends freeAPIService {
  // mock everything used by the component
};

describe('BulkUploadComponent', () => {
  let component: BulkUploadComponent;
  let fixture: ComponentFixture<BulkUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      //declarations: [ BulkUploadComponent ]
      declarations: [ BulkUploadComponent,HomeComponent,HeaderComponent ],
      imports: [ReactiveFormsModule, RouterTestingModule, HttpClientModule,
        HttpClientTestingModule,HttpModule,FormsModule],
      providers: [{
        provide: freeAPIService,
        useClass: MockDummyService
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
