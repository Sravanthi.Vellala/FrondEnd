import { Component, OnInit } from '@angular/core';
import { freeAPIService } from '../../services/freeapi.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-associate-details',
  templateUrl: './associate-details.component.html',
  styleUrls: ['./associate-details.component.css']
})
export class AssociateDetailsComponent implements OnInit {

  showMsg: boolean = false;
  constructor(private _freeApiService:freeAPIService)
  {
    this.resetForm();
  }

  ngOnInit() {
  }

  resetForm(form?:NgForm)
  {
    if(form!=null)
    {
    form.resetForm();
    this._freeApiService.associateDetails= {
     associateName:'',
     contactNumber:null,
     emaiId:'',
     employeeID:null,
     password:''
     //roleId:null
    }
    }
  }

  onSubmit(form:NgForm){
 this._freeApiService.postAssociateDetails(form.value).subscribe(res=>{
});
this.showMsg= true;
this.resetForm(form);
  }

}
