import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssoicateViewComponent } from './assoicate-view.component';

describe('AssoicateViewComponent', () => {
  let component: AssoicateViewComponent;
  let fixture: ComponentFixture<AssoicateViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssoicateViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssoicateViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
