import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { POCReportsComponent } from './pocreports.component';

describe('POCReportsComponent', () => {
  let component: POCReportsComponent;
  let fixture: ComponentFixture<POCReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ POCReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(POCReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
