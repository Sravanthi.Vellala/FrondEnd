import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { POCDashboardComponent } from './pocdashboard.component';

describe('POCDashboardComponent', () => {
  let component: POCDashboardComponent;
  let fixture: ComponentFixture<POCDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ POCDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(POCDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
