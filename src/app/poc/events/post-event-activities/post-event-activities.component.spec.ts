import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostEventActivitiesComponent } from './post-event-activities.component';

describe('PostEventActivitiesComponent', () => {
  let component: PostEventActivitiesComponent;
  let fixture: ComponentFixture<PostEventActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostEventActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostEventActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
