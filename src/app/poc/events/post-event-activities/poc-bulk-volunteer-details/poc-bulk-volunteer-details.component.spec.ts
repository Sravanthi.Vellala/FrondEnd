import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PocBulkVolunteerDetailsComponent } from './poc-bulk-volunteer-details.component';

describe('PocBulkVolunteerDetailsComponent', () => {
  let component: PocBulkVolunteerDetailsComponent;
  let fixture: ComponentFixture<PocBulkVolunteerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PocBulkVolunteerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PocBulkVolunteerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
