import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { VolunteerDetails } from '../../../../classes/volunteerdetails';
import { AssociateDetails } from '../../../../classes/associatedetails';
import { freeAPIService } from '../../../../services/freeapi.service';
import { EventDetails } from '../../../../classes/eventdetails';

@Component({
  selector: 'app-poc-single-volunteer-details',
  templateUrl: './poc-single-volunteer-details.component.html',
  styleUrls: ['./poc-single-volunteer-details.component.css']
})
export class PocSingleVolunteerDetailsComponent implements OnInit {

  eventList: Array<VolunteerDetails>;
  eventDate: string;
  volunteerDetail: VolunteerDetails;
  employeeID: Array<AssociateDetails>;
  showMsg: boolean = false;
  pocId:string;

  constructor(private _freeApiService: freeAPIService) {
    this.resetForm();
    this.pocId=this._freeApiService.getCurrentUserId();
  }

  ngOnInit() {
    
  }
  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
      
    }
  }
  getEventDate(event: any) {
    this.eventDate = event.target.value;
  }

  
  getEventlistbasedonDate() {
    this._freeApiService.getEventListBasedOnPocAndDate(this.pocId,this.eventDate)
      .subscribe(eventList => {
        this.eventList = eventList;
      });
  }

  onEventSelect(event: any) {
    //const eventId = this.eventList && this.eventList.find(c=>c.eventName === event.target.value).eventId;   
    var eventDetails = this.eventList && this.eventList.find(c => c.eventId == event.target.value);
    const eventId = (eventDetails != null) ? eventDetails.eventId : 0;

    this._freeApiService.getAssociateListDetails(eventId).subscribe(employeeID => {

      this.employeeID = employeeID;
    });;
  }

onVolunteerChange(event: any) {
    var eventDetails = this.eventList && this.eventList.find(c => c.employeeID == event.target.value);
    const eventId = (eventDetails != null) ? eventDetails.employeeID : 0;
  }
  

  onSubmit(form: NgForm) {
    this._freeApiService.postVolunteerDetails(form.value).subscribe(res => {
    });
    this.showMsg = true;
    this.resetForm(form);
  }



}
