import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PocSingleVolunteerDetailsComponent } from './poc-single-volunteer-details.component';

describe('PocSingleVolunteerDetailsComponent', () => {
  let component: PocSingleVolunteerDetailsComponent;
  let fixture: ComponentFixture<PocSingleVolunteerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PocSingleVolunteerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PocSingleVolunteerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
