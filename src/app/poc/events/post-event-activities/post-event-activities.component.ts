import { Component, OnInit } from '@angular/core';
import { freeAPIService } from '../../../services/freeapi.service';
import { VolunteerDetails } from 'src/app/classes/volunteerdetails';
import { AssociateDetails } from '../../../classes/associatedetails';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-post-event-activities',
  templateUrl: './post-event-activities.component.html',
  styleUrls: ['./post-event-activities.component.css']
})
export class PostEventActivitiesComponent implements OnInit {
    showMsg: boolean = false;
    eventId: Array<String>;
    associateListDetails: Array<String>;
    constructor(private _freeApiService: freeAPIService) {
      this.resetForm();
      // this._freeApiService.getEventListDetails()
      //   .subscribe(eventId => {
      //     this.eventId = eventId
      //   });
  
      // this._freeApiService.getAssociateListDetails()
      //   .subscribe(associateListDetails => {
      //     this.associateListDetails = associateListDetails
      //   });
    }
  
    ngOnInit() {
    }
  
    resetForm(form?: NgForm) {
      if (form != null) {
        form.resetForm();
        // this._freeApiService.getVolunteerDetails = {
        //   eventId: '',
        //   associateDetailsId: '',
        //   volunteerHours: '',
        //   travelHours: '',
        //   livesImpacted: '',
        //   employeeName: '',
        //   employeeID: '',
        //   participationID: ''
        // }
      }
    }
  
    onSubmit(form: NgForm) {
      this._freeApiService.postVolunteerDetails(form.value).subscribe(res => {
      });
      this.showMsg = true;
      this.resetForm(form);
    }
  }
  
